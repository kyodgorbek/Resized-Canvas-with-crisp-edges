# Resized-Canvas-with-crisp-edges
Resized Canvas

var $canvas = $("mycanvas"),
    Canvas = $canvas.width,
    ctx = canvas.getContext("2d");
  
  if (window.devicePixelRatio) {
    var pixelWidth = canvas.width,
        pixelHeight = canvas.height;
        
     canvas.width = pixelWidth * window.devicePixelRatio;
     canvas.height = pixelHeight *window.devicePixelRatio;
     
     $canvas.css({ width: pixelWidth, height: pixelHeight });
     ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
  }
